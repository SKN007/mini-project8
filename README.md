# Mini projrct 8

Rust Command-Line Tool with Testing

## Requiements

Rust command-line tool

Data ingestion/processing

Unit tests

## Steps

1. Create a rust project.

```
   cargo new mini-project8
```

2. Prepare some data for this project. I sumarized the champions of UCL from 2000 to 2023.

   ![img](d.png)
3. Implement the code in src/main.rs.
4. Build project

   ```
   cargo build
   ```
5. Test

   ```
   ./target/debug/mini-project8 2023
   ```


### Result display

![img](sc.png)
