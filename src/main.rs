use std::error::Error;
use std::fs::File;
use csv::ReaderBuilder;

// Define a struct to represent the championship
#[derive(Debug, PartialEq)]
struct Championship {
    year: String,
    champion: String,
}

impl Championship {
    // Function to create a new championship from CSV record
    fn new(record: csv::StringRecord) -> Championship {
        Championship {
            year: record.get(0).unwrap_or("").to_string(),
            champion: record.get(1).unwrap_or("").to_string(),
        }
    }
}

// Function to read championships from CSV and return vector of championships
fn read_championships_from_csv(file_path: &str) -> Result<Vec<Championship>, Box<dyn Error>> {
    let mut championships = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);
    for result in rdr.records() {
        let record = result?;
        let championship = Championship::new(record);
        championships.push(championship);
    }
    Ok(championships)
}

// Function to find championship by year
fn find_championship_by_year<'a>(championships: &'a [Championship], year: &str) -> Option<&'a Championship> {
    championships.iter().find(|c| c.year == year)
}

fn main() {
    // Read championships from CSV
    let championships = match read_championships_from_csv("./data/UCL.csv") {
        Ok(champs) => champs,
        Err(err) => {
            eprintln!("Error reading championships: {}", err);
            return;
        }
    };

    // Get year from command line argument
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <year>", args[0]);
        return;
    }
    let year = &args[1];

    // Find championship by year and print information
    match find_championship_by_year(&championships, year) {
        Some(championship) => println!("In {}, the champion was: {}", championship.year, championship.champion),
        None => println!("Championship information not found for year: {}", year),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_championship_by_year_existing() {
        let championships = vec![
            Championship { year: "2023".to_string(), champion: "Manchester City".to_string() },
            Championship { year: "2022".to_string(), champion: "Real Madrid".to_string() },
        ];

        assert_eq!(
            find_championship_by_year(&championships, "2022"),
            Some(&Championship { year: "2022".to_string(), champion: "Real Madrid".to_string() })
        );
    }

    #[test]
    fn test_find_championship_by_year_not_existing() {
        let championships = vec![
            Championship { year: "2023".to_string(), champion: "Manchester City".to_string() },
            Championship { year: "2022".to_string(), champion: "Real Madrid".to_string() },
        ];

        assert_eq!(find_championship_by_year(&championships, "2021"), None);
    }
}
